module.exports = {
  publicPath: process.env.BASE_URL,
  devServer: {
    port: 8080
  },
  lintOnSave: false
}
