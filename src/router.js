import Vue from 'vue'
import VueRouter from 'vue-router'
import todo from './views/ToDo'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'todo',
    component: todo
  },
  {
    path: '/todov1',
    name: 'todov1',
    component: () => import('./views/ToDoV1.vue')
  },
  {
    path: '/todov2',
    name: 'todov2',
    component: () => import('./views/ToDoV2.vue')
  },
  {
    path: '/todov3',
    name: 'todov3',
    component: () => import('./views/ToDoV3.vue')
  },
  {
    path:'complete',
    name:'complete',
    component: () => import('./views/Complete.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router
