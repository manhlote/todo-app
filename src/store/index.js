import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export const STORAGE_KEY= 'todoItems'
export default new Vuex.Store({
  state: {
    todoList:JSON.parse(localStorage.getItem(STORAGE_KEY) || JSON.stringify([]))
  },
  mutations:{
    todoList(state,todoList){
      state.todoList = todoList
    }
  },
  getters:{
    todoList(state){
      return state.todoList
    }
  }
})
