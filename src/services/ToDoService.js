export const STORAGE_KEY = 'todoItems'
import store from '@/store'
/**
 * object ToDoItem
 * string id - id of item
 * string content  - content of todo item
 * string createAt - time of create item
 * string completedAt - time of complete item
 * string deleteAt - time of delete
 */

export default {
    radomId() {
        return Math.random().toString(36).substr(2, 5)
    },
    formatDate() {
        var d = new Date;
        var dformat = [d.getMonth() + 1, d.getDate(), d.getFullYear()].join('/') + ' ' + [d.getHours(), d.getMinutes(), d.getSeconds()].join(':');
        return dformat;
    },
    setItem(value) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(value))
        store.commit("todoList", value)
    },
    removeItem() {
        localStorage.removeItem(STORAGE_KEY);
    },
    clearAllItems() {
        localStorage.clear();
    },
    saveAllItems(items) {
        this.setItem(items);
    },
    getAllData() {
        var todoList = localStorage.getItem(STORAGE_KEY);
        return JSON.parse(todoList || JSON.stringify([]))
    },
    getAllToDo() {
        return this.getAllData().filter(x => !x.deleteAt)
    },
    getAllComplete() {
        return this.getAllData().filter(x => x.completedAt)
    },
    getAllDelete() {
        return this.getAllData().filter(x => x.deleteAt)
    },
    updateItem(item) {
        var items = this.getAllToDo();
        var index = items.findIndex(x => x.id === item.itemId)
        if (index === -1) return;
        items.splice(index, 1, item);
        this.saveAllItems(items);
    },
    removeItem(itemId) {
        var items = this.getAllToDo();
        var index = items.findIndex(x => x.id === itemId);
        if (index === -1) return;
        items.splice(index, 1);
        this.saveAllItems(items)
    },
    deleteToDo(todo) {
        let todoList = this.getAllData();

        let item = todoList.find(x => x.id == todo.id)
        item.deleteAt = this.formatDate();

        // let index = todoList.findIndex(item => item.id == todo.id)
        // todo.deleteAt =  this.formatDate();
        // todoList.splice(index, 1, todo)

        localStorage.todoItems = JSON.stringify(todoList)
        store.commit('todoList', todoList)

    },
    revertToDo(todo) {
        let todoList = this.getAllData();

        let item = todoList.find(x => x.id == todo.id)
        item.deleteAt = null;
        localStorage.setItem(STORAGE_KEY, JSON.stringify(todoList))
        store.commit("todoList", todoList)
    },
    doneToDo(todo) {
        let todoList = this.getAllData();

        let item = todoList.find(x => x.id == todo.id)
        item.completedAt = !item.completedAt ? this.formatDate() : null


        localStorage.setItem(STORAGE_KEY, JSON.stringify(todoList))
        store.commit("todoList", todoList)
    },
    sortToDoList(todoList) {
        localStorage.setItem(STORAGE_KEY, JSON.stringify(todoList))
        store.commit("todoList", todoList)
    }

}